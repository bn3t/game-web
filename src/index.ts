import './styles.css';
import font_file from './MegamaxJones-elRm.ttf';
const SNAKE_SIZE = 15;

const constrain = (value: number, min: number, max: number) => (value < min ? min : value > max ? max : value);

interface Position {
  x: number;
  y: number;
}

function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}

class Food {
  private x: number;
  private y: number;

  constructor(width: number, height: number) {
    this.x = getRandomInt(width / SNAKE_SIZE) * SNAKE_SIZE;
    this.y = getRandomInt(height / SNAKE_SIZE) * SNAKE_SIZE;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.fillStyle = 'white';
    ctx.fillRect(this.x, this.y, SNAKE_SIZE, SNAKE_SIZE);
  }

  get position(): Position {
    return { x: this.x, y: this.y };
  }
}

class Snake {
  private x: number;
  private y: number;
  private width: number;
  private height: number;
  private xspeed = 1;
  private yspeed = 0;
  private total = 0;
  private tail: Position[] = [];

  constructor(x: number, y: number, width: number, height: number) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  move() {
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total > 0) {
      this.tail[this.total - 1] = { x: this.x, y: this.y };
    }
    this.x += this.xspeed * SNAKE_SIZE;
    this.y += this.yspeed * SNAKE_SIZE;
    this.x = constrain(this.x, 0, this.width - SNAKE_SIZE);
    this.y = constrain(this.y, 0, this.height - SNAKE_SIZE);
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.fillStyle = 'white';
    ctx.fillRect(this.x, this.y, SNAKE_SIZE, SNAKE_SIZE);
    for (let i = 0; i < this.tail.length; i++) {
      const el = this.tail[i];
      ctx.fillRect(el.x, el.y, SNAKE_SIZE, SNAKE_SIZE);
    }
  }

  direction(x: number, y: number) {
    this.xspeed = x;
    this.yspeed = y;
  }

  eat(food: Food) {
    if (this.x === food.position.x && this.y === food.position.y) {
      this.total++;
      return true;
    } else return false;
  }

  death() {
    for (let i = 0; i < this.tail.length; i++) {
      const el = this.tail[i];
      if (this.x === el.x && this.y === el.y) {
        this.total = 0;
        this.tail = [];
        return true;
      }
    }
    return false;
  }
}

class Game {
  private canvas: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D | null;
  private width: number;
  private height: number;
  private snake: Snake;
  private food: Food;
  private counter = 0;
  private lastDraw = 0;
  private gameStarted = false;

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');
    if (this.ctx) {
      this.ctx.font = '50px "8bits"';
    }
    this.width = canvas.width;
    this.height = canvas.height;
    this.snake = new Snake(this.width / 2, this.height / 2, this.width, this.height);
    this.food = new Food(this.width, this.height);
  }

  start() {
    this.gameStarted = true;
    requestAnimationFrame(this.update);
  }

  stop() {
    this.gameStarted = false;
  }

  onKeyPress(e: KeyboardEvent) {
    switch (e.key) {
      case 'ArrowUp':
        this.snake.direction(0, -1);
        return true;
      case 'ArrowDown':
        this.snake.direction(0, 1);
        return true;
      case 'ArrowLeft':
        this.snake.direction(-1, 0);
        return true;
      case 'ArrowRight':
        this.snake.direction(1, 0);
        return true;
    }
    return false;
  }

  update = (timestamp: number) => {
    if (timestamp - this.lastDraw > 160 && this.ctx) {
      this.lastDraw = timestamp;

      this.ctx?.clearRect(0, 0, this.width, this.height);
      this.snake.move();
      if (this.snake.eat(this.food)) {
        this.counter++;
        this.food = new Food(this.width, this.height);
      }
      if (this.snake.death()) {
        this.counter = 0;
      }
      this.snake.draw(this.ctx);
      this.food.draw(this.ctx);
      this.ctx.strokeStyle = 'white';
      this.ctx.fillText(`${this.counter}`, this.width - 90, 70);
    }

    if (this.gameStarted) {
      requestAnimationFrame(this.update);
    }
  };
}

let font = new FontFace('8bits', `url(${font_file})`);
font
  .load()
  .then(() => {
    //@ts-ignore
    document.fonts.add(font);
    const canvas = document.getElementById('game');
    const game = new Game(canvas as HTMLCanvasElement);

    const onStart = (e: Event) => {
      game.start();
      e.preventDefault();
    };

    const onStop = (e: Event) => {
      game.stop();
      e.preventDefault();
    };

    const onKeyPress = (e: KeyboardEvent) => {
      if (game.onKeyPress(e)) {
        e.preventDefault();
      }
    };

    document.getElementById('start')?.addEventListener('click', onStart);
    document.getElementById('stop')?.addEventListener('click', onStop);
    document.addEventListener('keydown', onKeyPress);
  })
  .catch((e) => console.error('Could not load font', e));
